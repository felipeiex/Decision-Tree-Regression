# Decision Tree Regression
## Non-linear and non-continous regression model

### What is Decision Trees?

> Decision Trees are an important type of algorithm for predictive modeling machine learning.
> The classical decision tree algorithms have been around for decades and modern variations like random forest are among the most powerful techniques available.
> In this post you will discover the humble decision tree algorithm known by it’s more modern name CART which stands for Classification And Regression Trees [1].




### An easy explanation

How you can see on image below [2], the main idea behind decision tree is a quite simple, the first step is to split your data in regions where each region is represented by its mean.
![N|Solid](Images/split.jpg)


You can build a tree using the regions that you got from data and using the mean for represented the data that become its region, as you can see on figure below [2].
![N|Solid](Images/tree.jpg)

If you wanna understand better the idea behind this, you can get more details at Machine Learning A-Z™ course on Udemy (https://www.udemy.com/machinelearning/)



### References:
[1] - https://machinelearningmastery.com/classification-and-regression-trees-for-machine-learning/

[2] - https://www.udemy.com/machinelearning/ 